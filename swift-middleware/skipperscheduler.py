import unittest

import webob 
import swift
import eventlet
from eventlet.semaphore import Semaphore
from swift.common.swob import Request, Response

class scheduler_middleware(object):

    def __init__(self, app, conf):
        self.app = app
        self.current_group = 0
        self.nactive = 0

        # constants
        self.NGROUPS = 16
        self.SWITCH_TIME = 5

        self.chunks_per_query = dict()
        self.query_wait_time = dict()
        self.query_progress = dict()

        # cond variables to sleep and wakeup threads whose data is on 
        # grp != current_grp
        self.grp_cond_var = []
        self.grp_chunk_cond_var = []
        self.nwaiting = []
        self.nwaiting_per_chunk = []
        for i in range(self.NGROUPS):
            self.grp_cond_var.append(Semaphore(0))
            self.grp_chunk_cond_var.append(dict())
            self.nwaiting.append(0)
            self.nwaiting_per_chunk.append(dict())

        # bookkeeping dses for scheduling algorithm
        self.query_stats = []
        self.chunk_stats = []
        for i in range(self.NGROUPS):
            self.query_stats.append(dict())
            self.chunk_stats.append(dict())

        # read in layout information and build a map
        layout_map = dict()
        with open("/etc/swift/layout.csv") as f:
            for line in f:
                fields = line.split(',')
                client_id = fields[0].strip()
                container_name = fields[1].strip()
                chunkid = fields[2].strip()
                groupid = fields[3].strip()

                if not client_id in layout_map:
                    layout_map[client_id] = dict()

                if not container_name in layout_map[client_id]:
                    layout_map[client_id][container_name] = dict()

                assert not chunkid in layout_map[client_id][container_name],\
                    "chunk " + chunkid +\
                    " already in contnr. " + container_name +\
                    " with groupid " + layout_map[client_id][container_name][chunkid]

                layout_map[client_id][container_name][chunkid] = groupid

                print "added chunk " + chunkid + " of container " +\
                    container_name + " to group " + groupid

        print ("skipper sched initialized")

    def fairquery_policy(self):

        # readjust the waiting time of queries 
        for q in self.query_progress:
            if self.query_progress[q] == True:
                self.query_wait_time[q] = 0
            else:
                self.query_wait_time[q] += 1

            # reset for next round
            self.query_progress[q] = False

        # now determine rank of each group
        rank = [0 for i in range(self.NGROUPS)]
        for i in range(self.NGROUPS):
            rank[i] = len(self.query_stats[i])

            for q in self.query_stats[i]:
                rank[i] += self.query_wait_time[q]

        # now find group with max rank
        max_rank = 0
        max_grp = 0
        for i in range(self.NGROUPS):

            print " group " + str(i) + " has rank " + str(rank[i])

            if rank[i] > max_rank and i != self.current_group:
                max_rank = rank[i]
                max_grp = i

        return max_grp

    def maxchunk_policy(self):
        max_waiting = 0
        max_grp = 0
        for i in range(self.NGROUPS):
            if (self.nwaiting[i] > max_waiting):
                max_waiting = self.nwaiting[i]
                max_grp = i

        return max_grp

    def maxquery_policy(self):
        max_waiting = 0
        max_grp = 0
        for i in range(self.NGROUPS):

            print " group " + str(i) + " has dict len " + \
                str(len(self.query_stats[i]))

            if (len(self.query_stats[i]) > max_waiting and \
                i != self.current_group):

                max_waiting = len(self.query_stats[i])
                max_grp = i

        return max_grp

    def __call__(self, environ, start_response):
        req = swift.common.swob.Request(environ)
        try:
            vrs, account, container, obj = req.split_path(1, 4, True)
        except ValueError:
            return self.app(req.environ, start_response)

        # Any other call, pass along to swift
        if req.method != 'GET' or container == None or obj == None:
            return self.app(req.environ, start_response)

        UUQID = req.headers["X-Object-Meta-Uuqid"]
        client_id = (UUQID.split('-'))[0]
        if '.' in obj:
            chunk_id = obj.split('.')[1]
        else
            chunk_id = '0'

        """
        fields = str(obj).split("_")
        group = int(fields[4])
        chunk = int(fields[3])
        table = int(fields[2])
        client = int(fields[1])

        """
        print "Skipper middleware got a get call:" + str(container) + '.' +\
            str(obj) + " UUQID " + UUQID + " group " + str(group) +\
            " chunk " + str(chunk) + " clientid " + client_id

        group = layout_map[client_id][container][chunk_id]
        chunk = str(obj)

        print "Skipper middleware got a get call:" + str(container) + '.' +\
            str(obj) + " UUQID " + UUQID + " found group " + str(group)

        # add this request to the group stats
        if not UUQID in self.query_stats[group]:
            self.query_stats[group][UUQID] = 0

        # Update per query statistics
        if not UUQID in self.query_wait_time:
            assert (not UUQID in self.query_progress), "UUQID in pg not in wt"

            self.query_wait_time[UUQID] = 0
            self.query_progress[UUQID] = False
            self.chunks_per_query[UUQID] = 0

        # update per group statistics
        if not chunk in self.chunk_stats[group]:
            self.chunk_stats[group][chunk] = 0
            self.grp_chunk_cond_var[group][chunk] = Semaphore(0)
            self.nwaiting_per_chunk[group][chunk] = 0

        self.query_stats[group][UUQID] += 1
        self.chunk_stats[group][chunk] += 1
        self.chunks_per_query[UUQID] += 1

        print "Group " + str(group) + " now has " +\
            str(self.query_stats[group][UUQID]) +\
            " chunks for query " + UUQID 

        print "Group " + str(group) + " now has " +\
            str(self.chunk_stats[group][chunk]) +\
                " chunks for chunk " + str(chunk)

        print "Query " + UUQID + " now has " +\
            str(self.chunks_per_query[UUQID]) + " chunks"

        # if someone is active, lets just sleep
        if self.nactive != 0:
            assert self.nactive == 1, "nactive more than 1"

            print "call " + str(container) + '.' + str(obj) +\
                ": sleeping as nactive is " + str(self.nactive) +\
                " cg " + str(self.current_group)

            self.nwaiting[group] += 1
            self.nwaiting_per_chunk[group][chunk] += 1
            #self.grp_cond_var[group].acquire()
            self.grp_chunk_cond_var[group][chunk].acquire()
            self.nwaiting[group] -= 1
            self.nwaiting_per_chunk[group][chunk] -= 1

        # we were woken up.
        assert self.nactive == 0, "nactive not 0 after wakeup"
        self.nactive += 1

        """
        " When we are woken up, we are guaranteed to be on our group
        " our caller was kind enough to switch
        " The only case when this doesn't happen is when we are the only 
        " request and the last request left swift in a diff. grp.
        """
        if group != self.current_group:
            if self.nwaiting[0] != 0 or len(set(self.nwaiting)) != 1:
                assert 0, "group " + group +"!= cg " +\
                    self.current_group + " and not only request"

            print("call " + str(container) + '.' + str(obj) +\
                ':' ' switching to group ' + str(group))

            self.current_group = 0xDEAD
            eventlet.sleep(self.SWITCH_TIME)
            self.current_group = group

        """
        " Current group has our data. Fetch data from swift entirely
        " WARNING: We could have used generators but we need expliclit control
        " over when the request is complete. Real solution is to implement
        " switching logic in proxy server itself. Until then, we have to 
        " bear this limitation and hope it doesn't cause swapping.
        "
        """
        body = self.app(environ, start_response)
        result = []
        [result.append(data) for data in body]

        print("call " + str(container) + '.' + str(obj) + ':' ' done')

        # mark our query as being serviced this round
        self.query_progress[UUQID] = True

        # sum of nwaiting per chunk should be equal to total
        assert sum(self.nwaiting_per_chunk[self.current_group].values()) == self.nwaiting[self.current_group], "mismatched nwaiting"    

        """
        " Now, we need to determine who runs next. If someone else is on the 
        " same group as us, just wake up. If not, wakeup a thread on the
        " group with the maximum number of pending requests
        """
        if self.nwaiting[self.current_group] > 0:

            #self.grp_cond_var[self.current_group].release()
            # we need to find the min key with non-zero waiting threads
            min_chunk = max(self.nwaiting_per_chunk[self.current_group])
            for c in self.nwaiting_per_chunk[self.current_group]:
                if self.nwaiting_per_chunk[self.current_group][c] > 0 and \
                    c < min_chunk:
                    min_chunk = c

            assert self.nwaiting_per_chunk[self.current_group][min_chunk] > 0,\
                "invalid min chunk. nwaiting 0"

            #min_chunk = min(self.grp_chunk_cond_var[self.current_group])

            self.grp_chunk_cond_var[self.current_group][min_chunk].release()

            print ("Woke up a thread waiting on chunk " + str(min_chunk) +\
                " cg " + str(self.current_group))
        else:
            """
            " No more active requests. Now we determine next grp to switch to 
            " and signal the threads corresponding to that group. 
            " They will wake up and perform a group switch 
            """

            max_grp = self.maxchunk_policy()
            #max_grp = self.maxquery_policy()
            #max_grp = self.fairquery_policy()

            if self.nwaiting[max_grp] > 0:

                assert max_grp != self.current_group,\
                    "invalid nonzero cg. Should have been caught earlier?"


                print("call " + str(container) + '.' + str(obj) +\
                    ':' ' switching to group ' + str(max_grp))

                eventlet.sleep(self.SWITCH_TIME)
                self.current_group = max_grp

                #self.grp_cond_var[max_grp].release()
                min_chunk = min(self.grp_chunk_cond_var[max_grp])

                self.grp_chunk_cond_var[max_grp][min_chunk].release()

                print("call " + str(container) + '.' + str(obj) +\
                    ':' ' signaling cond var of group ' + str(max_grp) +\
                    " chunk " + str(min_chunk))

        #finally, we are done
        self.nactive -= 1

        self.query_stats[group][UUQID] -= 1
        self.chunk_stats[group][chunk] -= 1
        self.chunks_per_query[UUQID] -= 1

        if self.query_stats[group][UUQID] == 0:
            print "Done handling the last object for query " + UUQID +\
                " group "+ str(group)

            self.query_stats[group].pop(UUQID, None)

        if self.chunk_stats[group][chunk] == 0:
            print "Done handling the last chunk for chunk " + str(chunk) +\
                " group "+ str(group)

            assert self.nwaiting_per_chunk[group][chunk] == 0,\
                "invalid nw/grp. Should have been 0."

            self.chunk_stats[group].pop(chunk, None)
            self.grp_chunk_cond_var[group].pop(chunk, None)
            self.nwaiting_per_chunk[group].pop(chunk, None)

        if self.chunks_per_query[UUQID] == 0:
            print "Done handling last object for query " + UUQID +\
                " across all groups"

            self.chunks_per_query.pop(UUQID, None)
            self.query_wait_time.pop(UUQID, None)
            self.query_progress.pop(UUQID, None)

        return result

def filter_factory(global_conf, **local_conf):
    conf = global_conf.copy()
    conf.update(local_conf)

    def scheduler_filter(app):
        return scheduler_middleware(app, conf)

    return scheduler_filter

if __name__ == '__main__':
    unittest.main()
