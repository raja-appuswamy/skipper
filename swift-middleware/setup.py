from distutils.core import setup

setup(name='skipper-scheduler',
      version='1.0',
      py_modules=['skipper-scheduler'],
      )
