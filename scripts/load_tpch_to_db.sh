#!/bin/bash

/usr/lib/postgresql/9.2.1/bin/psql -U postgres -d postgres -f tpch_create_tables.sql

declare -a tables=("nation" "region" "supplier" "part" "partsupp" "customer" "orders" "lineitem") 

DATA_DIR="/media/Data1/code/tpch/tpch_2_17_0/dbgen/10GB-data"

cd /media/Data1/code/data_loading/parallel-copy/

for table in "${tables[@]}"
do
#    echo sed -i 's/|$//' ${DATA_DIR}/${table}.tbl
#    sed -i 's/|$//' ${DATA_DIR}/${table}.tbl

#    echo ./pcopy ${DATA_DIR}/${table}.tbl 1 ${table}
#    ./pcopy ${DATA_DIR}/${table}.tbl 1 ${table} 1
    echo /usr/lib/postgresql/9.2.1/bin/psql -U postgres -d postgres -c "COPY $table FROM '$DATA_DIR/${table}.tbl' WITH (DELIMITER '|', FORMAT CSV);"
    /usr/lib/postgresql/9.2.1/bin/psql -U postgres -d postgres -c "COPY $table FROM '$DATA_DIR/${table}.tbl' WITH (DELIMITER '|', FORMAT CSV);"

done
