#!/bin/bash

if [ $# -lt 3 ]; then
    echo "host_preptest nvms base_cid mjoin(1 = mjoin, 0=no)"
	exit 1
fi

NVMS=$1
BASE_CID=$2
MJOIN_ON=$3
ips=$4
OPTFLAGS=$5
nips=1

#start vms
#i=1
#while [ $i -le $NVMS ]; do
#    virsh start pgvm$i
#    ((i+=1))
#done

#cp ~/.ssh/id_rsa.pub /tmp
#scp /tmp/id_rsa.pub ubuntu@$ip:
#rsh ubuntu@$ip -n "sudo cat id_rsa.pub > ~/.ssh/authorized_keys"

# get ips
#ips=`sudo python getips.py $NVMS`
#nips=`sudo python getips.py  $NVMS | wc -l`
#while [ $nips -ne $NVMS ];
#do
#	echo "waiting for ip assignment...($nips out of $NVMS)"
##sleep 1
#
#    ips=`sudo python getips.py $NVMS` 
#    nips=`sudo python getips.py $NVMS | wc -l`
#done

echo "ips acquired:" $ips

#prep and start services
CID=$BASE_CID
for ip in $ips
do
	#scp q5.sql ubuntu@$ip:
    scp "$OPTFLAGS" guest-prep.sh ubuntu@$ip:
    for qfile in `ls q*.sql`; do
        scp "$OPTFLAGS" $qfile ubuntu@$ip:
    done

    scp "$OPTFLAGS" guest-runtest.sh ubuntu@$ip:
    scp "$OPTFLAGS" ../set-swift-env.sh ubuntu@$ip:

    if [ $MJOIN_ON -eq 1 ]; then
        scp "$OPTFLAGS" mjoin-guest-start-services.sh ubuntu@$ip:guest-start-services.sh
        scp "$OPTFLAGS" guest-prep.sh ubuntu@$ip:
        scp "$OPTFLAGS" mjoin-postgresql.conf ubuntu@$ip:postgresql.conf
        scp "$OPTFLAGS" run_client_scheduler.sh ubuntu@$ip:/home/ubuntu/mjoin-files/client_scheduler/scheduler/run.sh
#        scp client_scheduler_mjoin.tar ubuntu@$ip:/home/ubuntu/mjoin-files/
#        scp smoothdb.tar.gz ubuntu@$ip:/home/ubuntu/mjoin-files/
    else
        scp "$OPTFLAGS" run_client_scheduler.sh ubuntu@$ip:/home/ubuntu/client_scheduler/scheduler/run.sh
        scp "$OPTFLAGS" guest-start-services.sh ubuntu@$ip:
        scp "$OPTFLAGS" postgresql.conf ubuntu@$ip:postgresql.conf
    fi


    #echo Prepping client $CID
    #rsh ubuntu@$ip -n "rm -rf mjoin-files/client_scheduler"
    #rsh ubuntu@$ip -n "cd mjoin-files && tar xf client_scheduler_mjoin.tar"
    #rsh ubuntu@$ip -n "cd mjoin-files/client_scheduler/scheduler && make clean && make"
#    rsh ubuntu@$ip -n "sh guest-prep.sh"

    #echo Starting services $CID
#    rsh ubuntu@$ip -n "sh guest-start-services.sh $CID > /dev/null < /dev/null &"

#    rsh ubuntu@$ip -n "ps ax | egrep '(client-scheduler|postgres|fusexmp)'"

    echo Done setting up client $CID

    CID=$((CID+1))
done

