#!/bin/bash

SF=5
NPARTS=5

cd /media/Data1/code/tpch/tpch_2_17_0/dbgen
for i in `seq 1 $NPARTS`; do ./dbgen -s $SF -S $i -C $NPARTS -v; done
mv *tbl* /media/Data2/data/coldstorage/tpch_partitions/
cd -
