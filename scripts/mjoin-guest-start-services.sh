#!/bin/bash

#PASS CLIENTID PGPATH AS ARGUMENT

if [ $# -lt 2 ]; then
	echo start CID PG_PATH
	exit 1
fi

CID=$1
PG_PATH=$2

cd /home/ubuntu/mjoin-files/client_scheduler/scheduler
sh run.sh $CID

sudo -u postgres /usr/local/mjoin-pgsql/bin/postgres -D $PG_PATH/data/ &


