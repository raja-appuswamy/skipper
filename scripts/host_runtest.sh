#!/bin/bash

NVMS=$1
BASE_CID=$2
QID=$3

if [ $# -lt 3 ]; then
	echo vm_runtest nvms base_cid query_id
	exit 1
fi

#start vms
#i=1
#while [ $i -le $NVMS ]; do
#    virsh start pgvm$i
#    ((i+=1))
#done

#cp ~/.ssh/id_rsa.pub /tmp
#scp /tmp/id_rsa.pub ubuntu@$ip:
#rsh ubuntu@$ip -n "sudo cat id_rsa.pub > ~/.ssh/authorized_keys"

# get ips
ips=`sudo python getips.py $NVMS`
nips=`sudo python getips.py  $NVMS | wc -l`
while [ $nips -ne $NVMS ];
do
	echo "waiting for ip assignment...($nips out of $NVMS)"
	sleep 1

    ips=`sudo python getips.py $NVMS` 
    nips=`sudo python getips.py $NVMS | wc -l`
done

echo "ips acquired:" $ips

echo Starting tests now...
CID=$BASE_CID
for ip in $ips 
do
    rsh ubuntu@$ip -n "ps ax | grep client"
    rsh ubuntu@$ip -n "ps ax | grep post"

    rsh ubuntu@$ip -n "sh guest-runtest.sh $QID > /dev/null < /dev/null &"
    echo Started test on client $CID
    CID=$((CID+1))
done

exit 0

#fetch results
#CID=$BASE_CID
#for ip in $ips 
#do
#    scp ubuntu@$ip:results.out ./results$CID.out
#    CID=$((CID+1))
#done

#for ip in $ips
#do
#	rsh postgres@$ip "sh guest_run_skipper_test.sh $NTHREADS" &
#done

#echo "loading started"

#wait

#for ip in $ips
#do
#	scp postgres@$ip:client_scheduler/scheduler/log.txt ./$ip.log.out
#done

