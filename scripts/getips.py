import subprocess
import sys

output = subprocess.check_output(["arp", "-an", "-i", "virbr0"])

mac2ip = dict()
for line in output.splitlines():
    fields = line.split(' ')
    mac2ip[fields[3]] = fields[1].strip("()")

vm_configs = []
for i in {1, int(sys.argv[1])}:
    vm_configs.append(subprocess.check_output(["virsh", "dumpxml", "pgvm" + str(i)]))

for mac in mac2ip:
    vmid = 2
    for config in vm_configs:
        if mac in config:
#            print "VM" + str(vmid), "mac ", mac, " ip ", mac2ip[mac]
            print mac2ip[mac]
        vmid += 1
            


