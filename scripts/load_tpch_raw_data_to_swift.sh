#!/bin/bash

#declare -a tables=("supplier" "lineitem" "part" "partsupp" "customer"  "orders")
declare -a tables=("lineitem")

export ST_AUTH=http://127.0.0.1:8080/auth/v1.0
export ST_USER=test:tester
export ST_KEY=testing

export AUTH_TOKEN=`(curl -v -H 'X-Storage-User: test:tester' -H 'X-Storage-Pass: testing' "http://127.0.0.1:8080/auth/v1.0") 2>&1 > /dev/null | grep "X-Auth-Token" | cut -d':' -f2 | tr -d '[:space:]'`

export STORAGE_URL='http://127.0.0.1:8080/v1/AUTH_test'

NCHUNKS=2
DATA_DIR="/media/Data2/data/coldstorage/tpch_partitions"

exit 0

cd $DATA_DIR

# create parent container
ROOT="tables"
curl -i -X PUT -H "x-auth-token: $AUTH_TOKEN" "$STORAGE_URL/$ROOT" 

#create segment holders
for table in "${tables[@]}"
do
	#create segment container
	curl -i -X PUT -H "x-auth-token: $AUTH_TOKEN" "$STORAGE_URL/${table}_chunks"

	for i in `seq 1 $NCHUNKS`; 
	do
		#push data
		curl -i -X PUT -H "x-auth-token: $AUTH_TOKEN" "$STORAGE_URL/${table}_chunks/${table}.tbl.$i" --data @${table}.tbl.$i

	done
done

# now formulate and upload manifest
for table in "${tables[@]}"
do
	MANIFEST="["

	for i in `seq 1 $NCHUNKS`; 
	do
		seg_path=/${table}_segments/${table}.tbl.$i

		ETAG=`swift stat ${table}_segments ${table}.tbl.$i | grep ETag | cut -d ':' -f2 | sed -e 's/^[ \t]*//'`

		SIZE=`swift stat ${table}_segments ${table}.tbl.$i | grep "Content Length" | cut -d ':' -f2 | sed -e 's/[ \t]*//'`

		SEGMENT="{\"path\":\"$seg_path\",\"etag\":\"$ETAG\",\"size_bytes\":$SIZE}"

		[ "$MANIFEST" != "[" ] && MANIFEST="$MANIFEST,"

		MANIFEST="$MANIFEST$SEGMENT"
	done

	MANIFEST="${MANIFEST}]"
	echo $MANIFEST

	# upload manifest with name <tablename.tbl> to root
	curl -i -X PUT -H "X-Auth-Token: $AUTH_TOKEN" "${STORAGE_URL}/${ROOT}/${table}.tbl?multipart-manifest=put" --data "$MANIFEST"

done

# upload small tables 
#swift upload "region" "region.tbl"
#swift upload "nation" "nation.tbl"

cd -


