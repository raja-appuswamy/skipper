
#declare -a tables=("supplier" "lineitem" "part" "partsupp" "customer"  "orders")
#declare -a tables=("lineitem")

export ST_AUTH="http://172.16.1.29:8080/auth/v1.0"
export ST_USER=test$1:tester$1
export ST_KEY=testing$1

export AUTH_TOKEN=`(curl -v -H "X-Storage-User: ${ST_USER}" -H "X-Storage-Pass: ${ST_KEY}" "http://172.16.1.29:8080/auth/v1.0") 2>&1 > /dev/null | grep "X-Auth-Token" | cut -d':' -f2 | tr -d '[:space:]'`

export STORAGE_URL="http://172.16.1.29:8080/v1/AUTH_test$1"

echo ST_USER is $ST_USER key is $ST_KEY url us $STORAGE_URL token is $AUTH_TOKEN
