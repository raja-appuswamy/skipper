#!/bin/bash
#assumes file name fits in 5 chars

DATA_DIR=/media/Data2/data/pgsql/swift_space/PG_9.2_201204301/12042/

. set-swift-env.sh $1

cd $DATA_DIR

for file in `ls`; do
    echo $i -> swift upload ${file:0:5} $file 
    swift upload ${file:0:5} $file
done

