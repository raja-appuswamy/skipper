#!/bin/bash

#set -x
#trap read debug

if [ $# -lt 1 ]; then
	echo "specify path to pg skeleton source (ex: /home/ubuntu/ssb-skeleton/pgsql/)"
	exit 1
fi

PG_PATH=$1
#PG_PATH=/home/ubuntu/mr-bench-skeleton/pgsql/

tblspc=`ls $PG_PATH/swift_space/PG_9.2_201204301/`

for i in `seq 1 3`; do
sudo kill -9 ` ps ax | grep client-scheduler | head -1 | tr -s '  ' ' ' | sed 's/^ //' | cut -d' ' -f1`

sudo kill -9 ` ps ax | grep fusexmp | head -1 | tr -s '  ' ' ' | sed 's/^ //' | cut -d' ' -f1`

sudo fusermount -u $PG_PATH/swift_space/PG_9.2_201204301/$tblspc

sudo kill -9 ` ps ax | grep "postgres -D" | head -1 | tr -s '  ' ' ' | sed 's/^ //' | cut -d' ' -f1`
sudo kill -9 ` ps ax | grep "postgres -D" | head -1 | tr -s '  ' ' ' | sed 's/^ //' | cut -d' ' -f1`

sleep 1
done

#setup swift space
set -e
sudo chmod -R ugo+rwx $PG_PATH/swift_space
cd $PG_PATH/swift_space/PG_9.2_201204301/$tblspc
sudo chmod ugo+rwx *
for file in `ls`; do >$file; done
cd -
set +e

# reset link
sudo rm /tmp/swift_space
sudo ln -s $PG_PATH/swift_space/ /tmp/swift_space
sudo chown postgres /tmp/swift_space

sudo chown -R postgres $PG_PATH

# setup fuse source
rm -rf /home/ubuntu/fuse-source
mkdir /home/ubuntu/fuse-source

cp $PG_PATH/swift_space/PG_9.2_201204301/$tblspc/* /home/ubuntu/fuse-source
sudo chown -R postgres /home/ubuntu/fuse-source


#echo 25769803776 | sudo tee /proc/sys/kernel/shmmax
echo 51539607552 | sudo tee /proc/sys/kernel/shmmax

#echo 6291456 | sudo tee /proc/sys/kernel/shmall
echo 12582912 | sudo tee /proc/sys/kernel/shmall


sudo cp /home/ubuntu/postgresql.conf $PG_PATH/data/
sudo chown postgres $PG_PATH/data/postgresql.conf
sudo chmod u=rw $PG_PATH/data/postgresql.conf
sudo chmod go= $PG_PATH/data/postgresql.conf

sudo chmod -R ugo+rw /home/ubuntu/*.out

for i in {1,2}; do     sudo ipcrm -Q `ipcs | tail -2 | head -1 | cut -d' ' -f1`; done
