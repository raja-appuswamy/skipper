#!/bin/bash

export Swift_Auth_Token=`(curl -v -H "X-Storage-User: test$1:tester$1" -H "X-Storage-Pass: testing$1" "http://172.16.1.29:8080/auth/v1.0") 2>&1 > /dev/null | grep "X-Auth-Token" | cut -d':' -f2 | tr -d '[:space:]'` 

export Swift_Storage_URL="http://172.16.1.29:8080/v1/AUTH_test$1"

for i in {1,2};
do
    sudo ipcrm -Q `ipcs | tail -2 | head -1 | cut -d' ' -f1`;
done

sudo rm /tmp/query.json /tmp/cli_key /tmp/srv_key

#valgrind --leak-check=yes ./client-scheduler
./client-scheduler $1 > sched.log 2>&1 &
