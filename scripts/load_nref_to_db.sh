#!/bin/bash

declare -a tables=("identical_seq" "neighboring_seq" "organism" "protein" "source" "taxonomy")

DATA_DIR="/media/Data1/data/nref"

cd $DATA_DIR

for table in "${tables[@]}"
do
#    echo sed -i 's/|$//' ${DATA_DIR}/${table}.tbl
#    sed -i 's/|$//' ${DATA_DIR}/${table}.tbl

#    echo ./pcopy ${DATA_DIR}/${table}.tbl 1 ${table}
#    ./pcopy ${DATA_DIR}/${table}.tbl 1 ${table} 1
    echo /usr/lib/postgresql/9.2.1/bin/psql -U postgres -d postgres -c "COPY $table FROM '$DATA_DIR/${table}.tbl' WITH (DELIMITER '|', FORMAT CSV);"
    /usr/lib/postgresql/9.2.1/bin/psql -U postgres -d postgres -c "COPY $table FROM '$DATA_DIR/${table}.txt' WITH (DELIMITER '|', FORMAT CSV);"

done
