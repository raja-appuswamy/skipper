if [ $# -lt 4 ]; then
    echo "./guest-runtest <cid> <num_iterations> <pfwindow> <path>"
    exit 1
fi

CID=$1
ITER=$2
WINDOW=$3
PG_PATH=$4
>results.out

for i in `seq 1 $ITER`; do
    ./guest-prep.sh $PG_PATH
    sleep 1
    ./guest-prep.sh $PG_PATH
    sleep 1
    ./guest-start-services.sh $CID $WINDOW $PG_PATH
    sleep 2

    echo "run $i" >> results.out
    date >> results.out
    time /usr/local/pgsql/bin/psql -d postgres -U postgres -f /home/ubuntu/query.sql -o query.out >> results.out 2>&1
    date >> results.out
done
