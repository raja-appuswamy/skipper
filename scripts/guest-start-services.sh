#!/bin/bash

#PASS CLIENTID WINDOW SIZE AS ARGUMENT. WINDOW SIZE HELPS ONLY WITH PF SCHED

if [ $# -lt 3 ]; then
	echo start CID WINDOW_SIZE PG_PATH
	exit 1
fi

CID=$1
PF_WINDOW=$2
PG_PATH=$3
tblspc=`ls $PG_PATH/swift_space/PG_9.2_201204301/`

echo using pg $PG_PATH

#cd /home/ubuntu/client_scheduler/prefetch_scheduler
cd /home/ubuntu/client_scheduler/scheduler
sh run.sh $CID $PF_WINDOW

cd /home/ubuntu/swift-fusefs/
sudo ./fusexmp -o modules=subdir,subdir=/home/ubuntu/fuse-source,allow_other,default_permissions,nonempty $PG_PATH/swift_space/PG_9.2_201204301/$tblspc

sudo -u postgres /usr/local/pgsql/bin/postgres -D $PG_PATH/data/ > pglog 2>&1 &


