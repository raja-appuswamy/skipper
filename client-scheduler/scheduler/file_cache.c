#include "scheduler.h"
#include "cache.h"
#include "queue.h"
#include <linux/limits.h>

/* Linked list of cached files entries */
static STAILQ_HEAD(tailhead, entry) head;
struct entry {
    char fname[PATH_MAX];

    STAILQ_ENTRY(entry) entries;
};

static pthread_mutex_t glock;

static int nentries;

void cache_init()
{
    STAILQ_INIT(&head);

    pthread_mutex_init(&glock, NULL);
}

struct entry *cache_evict()
{
    struct entry *e;

    assert(!STAILQ_EMPTY(&head));
    
    e = STAILQ_FIRST(&head);

    STAILQ_REMOVE_HEAD(&head, entries);

    nentries--;

    /* delete the file now */
    printf("Cache removing file %s\n", e->fname);

    assert(unlink(e->fname) == 0);

    return e;
}

/*
 * Check here to see if we have enough cache capacity to hold all 
 * the data * files requested by new threads. If not, free some files
 */
void cache_insert(char *file_name)
{
    struct entry *e;

    pthread_mutex_lock(&glock);

    if (nentries == CACHE_SIZE) {
        e = cache_evict();
    } else {
        e = malloc(sizeof(struct entry));
    }
    assert(e);

    strcpy(e->fname, file_name);

    printf("Cache: Inserting file %s\n", e->fname);

    STAILQ_INSERT_TAIL(&head, e, entries);

    nentries++;
    
    pthread_mutex_unlock(&glock);
}

/* 
 * Check to see if we already have the file. If so, udpate LRU list
 */
int cache_get(char *file_name)
{
    struct entry *e;

    pthread_mutex_lock(&glock);

    STAILQ_FOREACH(e, &head, entries) {
        //printf("Cache: get comparing %s - %s\n", e->fname, file_name);

        if (strcmp(e->fname, file_name) == 0) {

            /* deque and enque again */
            STAILQ_REMOVE(&head, e, entry, entries);
            STAILQ_INSERT_TAIL(&head, e, entries);

            pthread_mutex_unlock(&glock);

            return 1;
        }
    }

    pthread_mutex_unlock(&glock);

    return 0;
}
