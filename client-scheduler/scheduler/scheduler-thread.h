#ifndef _SCHEDULER_THREAD_H_
#define _SCHEDULER_THREAD_H_

#include "swift-client.h"

/* Types of test data with which to populate a Swift object */
enum test_data_type {
	SIMPLE_TEXT,  /* Simple text, easily identifiable in the Swift object's data */
	ALL_ZEROES,    /* Null bytes */
	PSEUDO_RANDOM /* Pseudo-random bits */
};

/**
 * In/out parameters to a Swift thread.
 */
struct scheduler_thread_args {
	swift_context_t swift;          /* Swift library context */
	unsigned int debug;             /* enable Swift client library dbg? */
	pthread_t thread_id;            /* pthread thread ID */
	unsigned int thread_num;        /* Swift thread index */
	char uuqid[16];      	        /* Univ. unique query id */

    const char *container_name;
    const char *object_name;
    char *file_name;

	enum swift_error scerr;         /* Swift client error encountered */

	struct timespec start_time;     /* Time of start of Swift thread */
	struct timespec end_time;       /* Time of end of Swift thread */
};

void *scheduler_thread_func(void *arg);

#endif
