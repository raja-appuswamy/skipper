#ifndef _CACHE_H
#define _CACHE_H

#define CACHE_SIZE 100 /* in units of chunk size whatever it is */

void cache_init();
void cache_insert(char *file_name);
int cache_get(char *file_name);

#endif
