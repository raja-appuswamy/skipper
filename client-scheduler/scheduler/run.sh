#!/bin/bash

export Swift_Auth_Token=`(curl -v -H 'X-Storage-User: test:tester' -H 'X-Storage-Pass: testing' "http://127.0.0.1:8080/auth/v1.0") 2>&1 > /dev/null | grep "X-Auth-Token" | cut -d':' -f2 | tr -d '[:space:]'`

export Swift_Storage_URL='http://127.0.0.1:8080/v1/AUTH_test'

for i in {1,2};
do
    sudo ipcrm -Q `ipcs | tail -2 | head -1 | cut -d' ' -f1`;
done

sudo rm /tmp/query.json /tmp/cli_key /tmp/srv_key

#valgrind --leak-check=yes ./client-scheduler
./client-scheduler 1 
