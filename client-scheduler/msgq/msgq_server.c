#include "msgq.h"
#include "msgq.c"

static int snd_msgqid, rcv_msgqid;

void msgq_server_init()
{
    key_t snd_key, rcv_key;
    int fd;
    mode_t mask;

    mask = umask(S_IXUSR | S_IXGRP | S_IXOTH);

    snd_key = msgq_create_key_file(FTOK_CLI_KEY);
    rcv_key = msgq_create_key_file(FTOK_SRV_KEY);

    snd_msgqid = msgq_init(snd_key, MSGQ_MODE | IPC_CREAT);
    rcv_msgqid = msgq_init(rcv_key, MSGQ_MODE | IPC_CREAT);

    /* create query json file if necessary */
    fd = open(TMP_FILE, O_RDWR | O_CREAT | O_EXCL, 
            S_IRWXU | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
    if (fd > 0)
        close(fd);            
    else
        perror("error opening query file.\n");

    umask(mask);
}

char *msgq_server_receive()
{
    int r;
    FILE *tmp;
    char *data, *fname;
    struct stat sbuf;

    /* get json file name */
    fname = msgq_receive(rcv_msgqid);
    printf("server rcv: \"%s\"\n", fname);

    /* read in json file */
    stat(fname, &sbuf);
    data = calloc(1, sbuf.st_size);
    assert(data);

    tmp = fopen(fname, "r");
    r = fread(data, 1, sbuf.st_size, tmp);
    assert (r == sbuf.st_size);

    printf("JSON data... %s\n", data);

    fclose(tmp);

    //r = unlink(fname);
    //assert (r == 0);

    free(fname);

    return data;
}

void msgq_server_send(const char *data)
{
    return msgq_send(snd_msgqid, data);
}
