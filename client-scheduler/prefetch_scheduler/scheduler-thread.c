#include <time.h>

#include "scheduler.h"
#include "cache.h"
#include "msgq.h"

//#define TMP_DIR "/mnt/ramdisk/ramspace/PG_9.2_201204301/12042"
#define TMP_DIR "/media/Data2/fuse/"

extern char *AUTH_TOKEN, *STORAGE_URL;

/**
 * Executed by each Swift thread.
 */
void *scheduler_thread_func(void *arg)
{
	struct scheduler_thread_args *args;
	int ret;
    wchar_t wcname[MAX_CNAME_LEN], woname[MAX_ONAME_LEN];

	const wchar_t *header_names[1] = {L"uuqid"};
	wchar_t *header_values[1];
	unsigned int nmetadata = 1;
	
	assert(arg != NULL);
	args = (struct scheduler_thread_args *) arg;

    /* Build output file name */

    if (strcmp(args->object_name, "0") == 0)
        sprintf(args->file_name, "%s/%s", TMP_DIR, args->container_name);
    else
        sprintf(args->file_name, "%s/%s", TMP_DIR, args->object_name);

	printf("Using outfile file %s\n", args->file_name);

    /* Check if we already have it cached */
    if (cache_get(args->file_name)) {
        printf("File %s found in cache..\n", args->file_name);
        fflush(stdout);

        return NULL;
    }

    printf("File %s not found in cache.. contacting swift..\n", 
            args->file_name);
    fflush(stdout);

    /* Now, add the file to the cache where it will be marked as pending */
    cache_insert(args->file_name);

    /* only time we will not find in cache is the first object of a table
     * First object always no segment suffix 
     */
    assert (strchr(args->object_name, '.') == NULL);

	args->scerr = swift_start(&args->swift);
	if (args->scerr != SCERR_SUCCESS) {
		return NULL;
	}

	ret = clock_gettime(CLOCK_MONOTONIC, &args->start_time);
    assert(ret == 0);

    args->scerr = swift_set_debug(&args->swift, args->debug);
    assert(args->scerr == SCERR_SUCCESS);

	args->scerr = swift_set_proxy(&args->swift, NULL);
    assert(args->scerr == SCERR_SUCCESS);

	args->scerr = swift_set_auth_token(&args->swift, AUTH_TOKEN);
    assert(args->scerr == SCERR_SUCCESS);

	args->scerr = swift_set_url(&args->swift, STORAGE_URL);
    assert(args->scerr == SCERR_SUCCESS);

    printf("Using container %s\n", args->container_name);

    ret = swprintf(wcname, MAX_CNAME_LEN, L"%s", args->container_name);
    if (ret < 0) {
        printf("Container name %s exceeded max length\n", 
            args->container_name);

        exit(1);
    }

	args->scerr = swift_set_container(&args->swift, wcname);
    assert(args->scerr == SCERR_SUCCESS);

    printf("Using object %s\n", args->object_name);

    ret = swprintf(woname, MAX_ONAME_LEN, L"%s", args->object_name);
    if (ret < 0) {
        printf("object name %s exceeded max length\n", args->object_name);
    }

    args->scerr = swift_set_object(&args->swift, woname);
    assert(args->scerr == SCERR_SUCCESS);

    printf ("using uuqid %s \n", args->uuqid);
    header_values[0] = malloc(sizeof(wchar_t) * strlen(args->uuqid));
    swprintf(header_values[0], sizeof(header_values[0]), 
            L"%s", args->uuqid);

    args->scerr = swift_get_file(&args->swift, args->file_name, 
            nmetadata, header_names, (const wchar_t **)header_values);

    assert(args->scerr == SCERR_SUCCESS);

    ret = clock_gettime(CLOCK_MONOTONIC, &args->end_time);
    if (ret != 0) {
		args->swift.errno_error("clock_gettime", errno);
		args->scerr = SCERR_INIT_FAILED; 
	}

	swift_end((swift_context_t *) arg);

    /* mark status as ready in cache */
    cache_update_status(args->file_name);

    free(header_values[0]);

	return NULL;
}
