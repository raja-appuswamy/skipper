#include "scheduler.h"
#include "cache.h"
#include "msgq.h"
#include "json_builder.h"

#define FAILURE 1

#define DEBUG 0

#define NULL_CHK(statement) \
	if ((statement) == NULL) \
		exit(FAILURE);

char *STORAGE_URL, *AUTH_TOKEN;

static double timespecs_to_microsecs(const struct timespec *start, 
		const struct timespec *end)
{
	double d = end->tv_sec - start->tv_sec;
	return d * 1000000 + (end->tv_nsec - start->tv_nsec) / 1000;
}

static void show_scheduler_times(
		const struct scheduler_thread_args *args, unsigned int n)
{
	fprintf(stderr, "Swift execution times for %u threads:\n", n);

	while (n--) {
		fprintf(stderr, 
				"Thread %3u:   get duration (microseconds): %12.3f\n", 
				args->thread_num, 
				timespecs_to_microsecs(&args->start_time, 
					&args->end_time));

		args++;
	}
}

int build_sched_args(jbuilder_t *builder,
        struct scheduler_thread_args **scheduler_args)
{
    int i, nargs, nalloc, len;
    json_object *item;

    *scheduler_args = calloc(1, sizeof(struct scheduler_thread_args));
    nargs = 0;
    nalloc = 1;

    json_object_object_foreach(builder->root, key, val) {
        assert (json_object_get_type(val) == json_type_array);
        len = json_object_array_length(val);

        for (i = 0; i < len; i++, nargs++) {
            if (nargs == nalloc) {

                *scheduler_args = realloc(*scheduler_args, 
                        nargs * 2 * sizeof(struct scheduler_thread_args));

                assert(*scheduler_args);

                nalloc = nargs * 2;
            }

            item = json_object_array_get_idx(val, i);
            assert(json_object_get_type(item) == json_type_string);

            memset(&((*scheduler_args)[nargs].swift), 0, 
                   sizeof(swift_context_t));

            (*scheduler_args)[nargs].debug = DEBUG;
            (*scheduler_args)[nargs].thread_num = nargs + 1;
            strcpy((*scheduler_args)[nargs].container_name, key);

            strcpy((*scheduler_args)[nargs].object_name,
                json_object_get_string(item));

        }
    }

    return nargs;
}

void *async_prefetch(void *arg)
{
#define PREFETCH_WINDOW_SIZE 2

    struct scheduler_thread_args scheduler_arg, *async_args;
    static int last_window;
    int oid, i, ret;
    char *tmp;

    /* copy over args variable as parent can free it any time */
    memcpy(&scheduler_arg, arg, sizeof(struct scheduler_thread_args));

    oid = 0;
    if ((tmp = strchr(scheduler_arg.object_name, '.')))
        oid = atoi(tmp + 1);

    /* if first request for a table, last window must be reset */
    if (oid == 0)
        last_window = 0;

    if (oid < last_window)
        return NULL;

    assert (oid == last_window);

    /* if this is first prefetch call, then reset last window to 1. Otherwise
     * we reset it by window size
     */
    if (!last_window)
        last_window = 1;
    else
        last_window += PREFETCH_WINDOW_SIZE;

    /* time to prefetch */
    async_args = calloc(PREFETCH_WINDOW_SIZE, 
            sizeof(struct scheduler_thread_args));

    for (i = 0; i < PREFETCH_WINDOW_SIZE; i++) {
        async_args[i].debug = DEBUG;
        async_args[i].thread_num = i + 1;
        strcpy(async_args[i].uuqid, scheduler_arg.uuqid);
        strcpy(async_args[i].container_name, 
                scheduler_arg.container_name);

        sprintf(async_args[i].object_name, "%s.%d", 
                scheduler_arg.container_name, i + last_window);

        printf("Async prefetching container %s object %s\n", 
                async_args[i].container_name, async_args[i].object_name);

        ret = pthread_create(&async_args[i].thread_id, NULL, 
                scheduler_thread_func, &async_args[i]);

        if (ret != 0) {
            perror("pthread_create");
            exit(EXIT_FAILURE);
        }
    }

    /* now we wait */
    for (i = 0; i < PREFETCH_WINDOW_SIZE; i++) {
        ret = pthread_join(async_args[i].thread_id, NULL);
        if (ret != 0) {
            perror("pthread_join");
            exit(EXIT_FAILURE);
        }
    }

    free(async_args);

    return NULL;
}

int main(int argc, char *argv[])
{
	struct scheduler_thread_args *scheduler_args;
	int i, nscheduler_threads, ret, CID, QID;
    char *msg;
    jbuilder_t *builder;
    pthread_t async_tid;

	STORAGE_URL = getenv("Swift_Storage_URL");
	AUTH_TOKEN = getenv("Swift_Auth_Token");

	if (!STORAGE_URL || !AUTH_TOKEN) {
		printf("Swift_Storage_URL or Swift_Auth_Token envvar unset.\n");
		exit(1);
	}

    printf("Using URL %s TOKEN %s\n", STORAGE_URL, AUTH_TOKEN);

    if (argc < 2) {
        printf("./cli_scheduler <client_id>\n");
        exit(1);
    }

    CID = atoi(argv[1]);
    QID = 0;

    printf("Using client ID %d\n", CID);

    // init swift
	if (swift_global_init() != SCERR_SUCCESS) {
		return EXIT_FAILURE;
	}
	atexit(swift_global_cleanup);

    // initialize commn. channels
    msgq_server_init();

    // initialize cache
    cache_init();

    while (1) {
        /* wait for message */
        msg = msgq_server_receive();

        /* deser it */
        builder = jbuilder_init(msg);

        printf("server got msg: %s\n", jbuilder_serialize(builder));

        scheduler_args = NULL;
	    nscheduler_threads = build_sched_args(builder, &scheduler_args);

        /* we are going to get request for only one block as this sched
         * will be used only with default pg. We are going to deal with
         * chunks of blocks which form prefetch window. 
         * The first request of each table will find the block missing.
         * In this case, we need to fetch the whole window.
         * The latter requests will always find the block cached because
         * we trigger async prefetching of next window when current
         * window is accessed.
         * Either way, steps are as follows:
         * 1) do current request first. data will either come from cache 
         * or be synch. fetched from swift
         * 2) send a message back so that pg can continue
         * 3) if necessary, asynchronously fetch prefetch window
         *
         * If necessary part:
         * The moment we access the first block of a new window (which 
         * was prefetched in the next round), we trigger an async prefetch
         */
        assert(nscheduler_threads == 1);

        /* generate UUID for each query */
        QID++;

	    for (i = 0; i < nscheduler_threads; i++) {

            snprintf(scheduler_args[i].uuqid, 
                    sizeof(scheduler_args[i].uuqid), "%d-%d", CID, QID);

		    ret = pthread_create(&scheduler_args[i].thread_id, NULL, 
				    scheduler_thread_func, &scheduler_args[i]);

		    if (ret != 0) {
			    perror("pthread_create");
			    return EXIT_FAILURE;
		    }
        }

        /* now queue async requests before we wait for sync thread if we 
         * are at the start of a prefetch window
         */
        ret = pthread_create(&async_tid, NULL, 
				    async_prefetch, &scheduler_args[0]);
        assert(ret == 0);

        pthread_detach(async_tid);

        /* wait for main thread. This should be pretty quick if cached */
	    for (i = 0; i < nscheduler_threads; i++) {
		    ret = pthread_join(scheduler_args[i].thread_id, NULL);
		    if (ret != 0) {
			    perror("pthread_join");
			    return EXIT_FAILURE;
		    }

            msgq_server_send(scheduler_args[i].file_name);
        }

	    show_scheduler_times(scheduler_args, nscheduler_threads);

	    free(scheduler_args);
        jbuilder_cleanup(builder);
        free(msg);
	}



	return 0;
}
