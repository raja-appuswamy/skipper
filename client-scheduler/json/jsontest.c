#include "json_builder.h"

int main(int argc, char *argv[]) {

    jbuilder_t *builder;
    container_t *c;

    builder = jbuilder_init(NULL);

    c = jbuilder_create_container(builder, "lineitem_segments");
    jbuilder_add_object(builder, c, "lineitem.tbl.1");
    jbuilder_add_object(builder, c, "lineitem.tbl.2");

    printf("JSON query is %s\n", jbuilder_serialize(builder));
            
    jbuilder_cleanup(builder);

    return 0;
}

