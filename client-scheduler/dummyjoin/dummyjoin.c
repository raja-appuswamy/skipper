#include "msgq.h"
#include "json_builder.h"
#include <libgen.h>

#define NOBJECTS 56

void generate_workload_all_chunks(jbuilder_t *builder)
{
    container_t *c;

	c = jbuilder_create_container(builder, "relation_10_0");
	jbuilder_add_object(builder, c, "relation_10_0_0_3");
	jbuilder_add_object(builder, c, "relation_10_0_1_3");
	c = jbuilder_create_container(builder, "relation_10_1");
	jbuilder_add_object(builder, c, "relation_10_1_0_0");
	jbuilder_add_object(builder, c, "relation_10_1_1_0");
	jbuilder_add_object(builder, c, "relation_10_1_2_1");
	c = jbuilder_create_container(builder, "relation_10_2");
	jbuilder_add_object(builder, c, "relation_10_2_0_4");
	jbuilder_add_object(builder, c, "relation_10_2_1_1");
	jbuilder_add_object(builder, c, "relation_10_2_2_6");

	c = jbuilder_create_container(builder, "relation_1_1");
	jbuilder_add_object(builder, c, "relation_1_1_0_5");
	jbuilder_add_object(builder, c, "relation_1_1_1_1");
	c = jbuilder_create_container(builder, "relation_1_0");
	jbuilder_add_object(builder, c, "relation_1_0_0_3");
	jbuilder_add_object(builder, c, "relation_1_0_1_0");
	c = jbuilder_create_container(builder, "relation_1_2");
	jbuilder_add_object(builder, c, "relation_1_2_0_2");
	jbuilder_add_object(builder, c, "relation_1_2_1_1");
	jbuilder_add_object(builder, c, "relation_1_2_2_3");

	c = jbuilder_create_container(builder, "relation_2_2");
	jbuilder_add_object(builder, c, "relation_2_2_0_0");
	jbuilder_add_object(builder, c, "relation_2_2_1_2");
	c = jbuilder_create_container(builder, "relation_2_0");
	jbuilder_add_object(builder, c, "relation_2_0_0_5");
	jbuilder_add_object(builder, c, "relation_2_0_1_5");
	jbuilder_add_object(builder, c, "relation_2_0_2_0");
	jbuilder_add_object(builder, c, "relation_2_0_3_3");
	c = jbuilder_create_container(builder, "relation_2_1");
	jbuilder_add_object(builder, c, "relation_2_1_0_0");
	jbuilder_add_object(builder, c, "relation_2_1_1_1");
	jbuilder_add_object(builder, c, "relation_2_1_2_0");
	jbuilder_add_object(builder, c, "relation_2_1_3_6");

	c = jbuilder_create_container(builder, "relation_3_2");
	jbuilder_add_object(builder, c, "relation_3_2_0_0");
	jbuilder_add_object(builder, c, "relation_3_2_1_1");
	c = jbuilder_create_container(builder, "relation_3_1");
	jbuilder_add_object(builder, c, "relation_3_1_0_3");
	jbuilder_add_object(builder, c, "relation_3_1_1_1");
	jbuilder_add_object(builder, c, "relation_3_1_2_1");
	jbuilder_add_object(builder, c, "relation_3_1_3_6");
	c = jbuilder_create_container(builder, "relation_3_0");
	jbuilder_add_object(builder, c, "relation_3_0_0_8");
	jbuilder_add_object(builder, c, "relation_3_0_1_1");
	jbuilder_add_object(builder, c, "relation_3_0_2_4");
	jbuilder_add_object(builder, c, "relation_3_0_3_4");

	c = jbuilder_create_container(builder, "relation_4_0");
	jbuilder_add_object(builder, c, "relation_4_0_0_0");
	jbuilder_add_object(builder, c, "relation_4_0_1_5");
	jbuilder_add_object(builder, c, "relation_4_0_2_7");
	c = jbuilder_create_container(builder, "relation_4_1");
	jbuilder_add_object(builder, c, "relation_4_1_0_0");
	jbuilder_add_object(builder, c, "relation_4_1_1_1");
	jbuilder_add_object(builder, c, "relation_4_1_2_0");
	c = jbuilder_create_container(builder, "relation_4_2");
	jbuilder_add_object(builder, c, "relation_4_2_0_5");
	jbuilder_add_object(builder, c, "relation_4_2_1_0");

	c = jbuilder_create_container(builder, "relation_5_1");
	jbuilder_add_object(builder, c, "relation_5_1_0_2");
	c = jbuilder_create_container(builder, "relation_5_0");
	jbuilder_add_object(builder, c, "relation_5_0_0_0");
	c = jbuilder_create_container(builder, "relation_5_2");
	jbuilder_add_object(builder, c, "relation_5_2_0_0");
	jbuilder_add_object(builder, c, "relation_5_2_1_2");

	c = jbuilder_create_container(builder, "relation_6_2");
	jbuilder_add_object(builder, c, "relation_6_2_0_8");
	jbuilder_add_object(builder, c, "relation_6_2_1_0");
	jbuilder_add_object(builder, c, "relation_6_2_2_4");
	jbuilder_add_object(builder, c, "relation_6_2_3_1");
	c = jbuilder_create_container(builder, "relation_6_0");
	jbuilder_add_object(builder, c, "relation_6_0_0_0");
	jbuilder_add_object(builder, c, "relation_6_0_1_0");
	jbuilder_add_object(builder, c, "relation_6_0_2_2");
	jbuilder_add_object(builder, c, "relation_6_0_3_7");
	c = jbuilder_create_container(builder, "relation_6_1");
	jbuilder_add_object(builder, c, "relation_6_1_0_6");
	jbuilder_add_object(builder, c, "relation_6_1_1_1");
	jbuilder_add_object(builder, c, "relation_6_1_2_0");
	jbuilder_add_object(builder, c, "relation_6_1_3_6");

	c = jbuilder_create_container(builder, "relation_7_2");
	jbuilder_add_object(builder, c, "relation_7_2_0_0");
	c = jbuilder_create_container(builder, "relation_7_1");
	jbuilder_add_object(builder, c, "relation_7_1_0_1");
	jbuilder_add_object(builder, c, "relation_7_1_1_0");
	jbuilder_add_object(builder, c, "relation_7_1_2_4");
	c = jbuilder_create_container(builder, "relation_7_0");
	jbuilder_add_object(builder, c, "relation_7_0_0_1");
	jbuilder_add_object(builder, c, "relation_7_0_1_1");

	c = jbuilder_create_container(builder, "relation_8_0");
	jbuilder_add_object(builder, c, "relation_8_0_0_0");
	jbuilder_add_object(builder, c, "relation_8_0_1_0");
	c = jbuilder_create_container(builder, "relation_8_1");
	jbuilder_add_object(builder, c, "relation_8_1_0_1");
	jbuilder_add_object(builder, c, "relation_8_1_1_5");
	c = jbuilder_create_container(builder, "relation_8_2");
	jbuilder_add_object(builder, c, "relation_8_2_0_1");
	jbuilder_add_object(builder, c, "relation_8_2_1_0");
	jbuilder_add_object(builder, c, "relation_8_2_2_3");
	jbuilder_add_object(builder, c, "relation_8_2_3_2");

	c = jbuilder_create_container(builder, "relation_9_1");
	jbuilder_add_object(builder, c, "relation_9_1_0_2");
	jbuilder_add_object(builder, c, "relation_9_1_1_1");
	c = jbuilder_create_container(builder, "relation_9_0");
	jbuilder_add_object(builder, c, "relation_9_0_0_7");
	jbuilder_add_object(builder, c, "relation_9_0_1_7");
	jbuilder_add_object(builder, c, "relation_9_0_2_0");
	c = jbuilder_create_container(builder, "relation_9_2");
	jbuilder_add_object(builder, c, "relation_9_2_0_1");
	jbuilder_add_object(builder, c, "relation_9_2_1_1");
	jbuilder_add_object(builder, c, "relation_9_2_2_0");
	jbuilder_add_object(builder, c, "relation_9_2_3_0");

}

// NOBJECTS here is 82
void generate_workload_micro1(jbuilder_t *builder)
{
    container_t *c;

	c = jbuilder_create_container(builder, "relation_10_1");
	jbuilder_add_object(builder, c, "relation_10_1_0_0");
	jbuilder_add_object(builder, c, "relation_10_1_1_0");
	jbuilder_add_object(builder, c, "relation_10_1_2_1");
	c = jbuilder_create_container(builder, "relation_10_2");
	jbuilder_add_object(builder, c, "relation_10_2_0_4");
	jbuilder_add_object(builder, c, "relation_10_2_1_1");
	jbuilder_add_object(builder, c, "relation_10_2_2_6");

	c = jbuilder_create_container(builder, "relation_1_1");
	jbuilder_add_object(builder, c, "relation_1_1_0_5");
	jbuilder_add_object(builder, c, "relation_1_1_1_1");
	c = jbuilder_create_container(builder, "relation_1_0");
	jbuilder_add_object(builder, c, "relation_1_0_0_3");
	jbuilder_add_object(builder, c, "relation_1_0_1_0");

	c = jbuilder_create_container(builder, "relation_2_0");
	jbuilder_add_object(builder, c, "relation_2_0_0_5");
	jbuilder_add_object(builder, c, "relation_2_0_1_5");
	jbuilder_add_object(builder, c, "relation_2_0_2_0");
	jbuilder_add_object(builder, c, "relation_2_0_3_3");
	c = jbuilder_create_container(builder, "relation_2_1");
	jbuilder_add_object(builder, c, "relation_2_1_0_0");
	jbuilder_add_object(builder, c, "relation_2_1_1_1");
	jbuilder_add_object(builder, c, "relation_2_1_2_0");
	jbuilder_add_object(builder, c, "relation_2_1_3_6");

	c = jbuilder_create_container(builder, "relation_3_1");
	jbuilder_add_object(builder, c, "relation_3_1_0_3");
	jbuilder_add_object(builder, c, "relation_3_1_1_1");
	jbuilder_add_object(builder, c, "relation_3_1_2_1");
	jbuilder_add_object(builder, c, "relation_3_1_3_6");
	c = jbuilder_create_container(builder, "relation_3_0");
	jbuilder_add_object(builder, c, "relation_3_0_0_8");
	jbuilder_add_object(builder, c, "relation_3_0_1_1");
	jbuilder_add_object(builder, c, "relation_3_0_2_4");
	jbuilder_add_object(builder, c, "relation_3_0_3_4");

	c = jbuilder_create_container(builder, "relation_4_0");
	jbuilder_add_object(builder, c, "relation_4_0_0_0");
	jbuilder_add_object(builder, c, "relation_4_0_1_5");
	jbuilder_add_object(builder, c, "relation_4_0_2_7");
	c = jbuilder_create_container(builder, "relation_4_1");
	jbuilder_add_object(builder, c, "relation_4_1_0_0");
	jbuilder_add_object(builder, c, "relation_4_1_1_1");
	jbuilder_add_object(builder, c, "relation_4_1_2_0");

	c = jbuilder_create_container(builder, "relation_5_1");
	jbuilder_add_object(builder, c, "relation_5_1_0_2");
	c = jbuilder_create_container(builder, "relation_5_0");
	jbuilder_add_object(builder, c, "relation_5_0_0_0");

	c = jbuilder_create_container(builder, "relation_6_0");
	jbuilder_add_object(builder, c, "relation_6_0_0_0");
	jbuilder_add_object(builder, c, "relation_6_0_1_0");
	jbuilder_add_object(builder, c, "relation_6_0_2_2");
	jbuilder_add_object(builder, c, "relation_6_0_3_7");
	c = jbuilder_create_container(builder, "relation_6_1");
	jbuilder_add_object(builder, c, "relation_6_1_0_6");
	jbuilder_add_object(builder, c, "relation_6_1_1_1");
	jbuilder_add_object(builder, c, "relation_6_1_2_0");
	jbuilder_add_object(builder, c, "relation_6_1_3_6");

	c = jbuilder_create_container(builder, "relation_7_1");
	jbuilder_add_object(builder, c, "relation_7_1_0_1");
	jbuilder_add_object(builder, c, "relation_7_1_1_0");
	jbuilder_add_object(builder, c, "relation_7_1_2_4");
	c = jbuilder_create_container(builder, "relation_7_0");
	jbuilder_add_object(builder, c, "relation_7_0_0_1");
	jbuilder_add_object(builder, c, "relation_7_0_1_1");

	c = jbuilder_create_container(builder, "relation_8_0");
	jbuilder_add_object(builder, c, "relation_8_0_0_0");
	jbuilder_add_object(builder, c, "relation_8_0_1_0");
	c = jbuilder_create_container(builder, "relation_8_1");
	jbuilder_add_object(builder, c, "relation_8_1_0_1");
	jbuilder_add_object(builder, c, "relation_8_1_1_5");

	c = jbuilder_create_container(builder, "relation_9_1");
	jbuilder_add_object(builder, c, "relation_9_1_0_2");
	jbuilder_add_object(builder, c, "relation_9_1_1_1");
	c = jbuilder_create_container(builder, "relation_9_0");
	jbuilder_add_object(builder, c, "relation_9_0_0_7");
	jbuilder_add_object(builder, c, "relation_9_0_1_7");
	jbuilder_add_object(builder, c, "relation_9_0_2_0");
}

int main(void)
{
    jbuilder_t *builder;
    int i;
    char *fname;
    char cmd[512];

    const char *json_data;

    builder = jbuilder_init(NULL);
    msgq_client_init();

    // get queries in json format
	generate_workload_micro1(builder);
    json_data = jbuilder_serialize(builder);
    printf("json is %s\n", json_data);

    // serialize file and send it to scheduler
    msgq_client_send(json_data);

    // now get file name one by one
    for (i = 0; i < NOBJECTS; i++) {
        fname = msgq_client_receive();

        printf("Got file %s\n", fname);

        sprintf(cmd, "./pqtest %s", basename(fname));

        printf("Issuing call %s\n", cmd);

        //system(cmd);

        free(fname);
    }

    msgq_client_disconnect();

    return 0;
}
