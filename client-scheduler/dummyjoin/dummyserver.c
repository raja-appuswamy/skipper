#include "msgq.h"
#include "json_builder.h"

int main(int argc, char *argv[])
{
    char *msg;
    jbuilder_t *builder;

    msgq_server_init();

    msg = msgq_server_receive();

    printf("server got msg: %s\n", msg);

    builder = jbuilder_init(msg);

    printf("serialized msg: %s\n", jbuilder_serialize(builder));

    jbuilder_cleanup(builder);

    return 0;
}
