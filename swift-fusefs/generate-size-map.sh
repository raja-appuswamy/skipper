#!/bin/bash

MAX_CHUNKS=16
echo "#define MAX_CHUNKS $MAX_CHUNKS"

echo "struct container {"
echo -e "\tchar *name;"
echo -e "\toff_t size[MAX_CHUNKS];"
echo -e "};\n"

echo "struct container containers[] = {"

for c in `swift list`; do 
    echo -e -n "\t" \{ \"$c\"", {"; 
    i=1
    
    for size in `swift list $c --long | head --lines=-1 | tr -s '  ' ' ' | cut -d' ' -f2`; do 
        echo -n "$size, "; 
        ((i+=1))
    done; 

    while [ $i -lt $MAX_CHUNKS ]
    do
        echo -n "-1, ";
        ((i+=1))
    done

    echo -n "-1 }"

    echo "},";
done

echo "};"
