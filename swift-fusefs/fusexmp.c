#define FUSE_USE_VERSION 26

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef linux
/* For pread()/pwrite()/utimensat() */
#define _XOPEN_SOURCE 700
#endif

#include "msgq.h"
#include "json_builder.h"
#include <libgen.h>
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <ctype.h>
#include <sys/time.h>
#ifdef HAVE_SETXATTR
#include <sys/xattr.h>
#endif

#define RETRY(func, ...) \
    { \
        res = func(__VA_ARGS__); \
        if (res == -1) {\
            swift_fetch(path); \
            fprintf(stderr, "Reissusing %s now..\n", __FUNCTION__); \
            res = func(__VA_ARGS__); \
            if (res == -1) \
                return -errno; \
        } \
    }

#include "chunk-size-map.h"

static void swift_fetch(const char *path)
{
    container_t *c;
    jbuilder_t *builder;
    char *fname, *bname, *cname, *oname, *ret;
    const char *json_data;

    fname = calloc(1, strlen(path) + 1);
    assert(fname);

    strcpy(fname, path);
    bname = basename(fname); 

    /* HACK - if we're not looking for anything but filenode numbers
     * just return.
     */
    if (!isdigit(bname[0])) {
        fprintf(stderr, "%s not file node. skipping switch ctct\n", 
                bname);

        free(fname);

        return;
    }

    cname = calloc(1, strlen(bname) + 1);
    assert(cname);

    strcpy(cname, bname);

    if ((oname = strchr(cname, '.'))) {
        *oname = '\0';
    }

    fprintf(stderr, "Using container %s object %s\n", cname, bname);

    builder = jbuilder_init(NULL);

    c = jbuilder_create_container(builder, cname);
    jbuilder_add_object(builder, c, bname);
	
    json_data = jbuilder_serialize(builder);
    fprintf(stderr, "json is %s\n", json_data);

    msgq_client_send(json_data);

    ret = msgq_client_receive();
    fprintf(stderr, "got file %s\n", ret);

    free(ret);  

    jbuilder_cleanup(builder);

    free(cname);
    free(fname);
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
	int res, i, oid;
    char *fname, *bname, *cname, *oname;

    //fprintf(stderr, "DBG: getattr %s\n", path);

	res = lstat(path, stbuf);
	if (res == -1) {
		return -errno;
    }

    // if size is already nonzero, don't bother
    if (stbuf->st_size)
        return 0; 

    // go through container list, find right one, and return right size
    fname = calloc(1, strlen(path) + 1);
    assert(fname);

    strcpy(fname, path);
    bname = basename(fname); 

    // if we hit on of the _fsm nodes or some other filenode, skip
    if (!isdigit(bname[0]) || (strchr(bname, '_') != NULL)) {
        fprintf(stderr, "%s not file node. skipping getattr check\n", 
                bname);

        free(fname);

        return 0;
    }

    cname = calloc(1, strlen(bname) + 1);
    assert(cname);

    strcpy(cname, bname);

    oid = 0;
    if ((oname = strchr(cname, '.'))) {
        *oname = '\0';
        oname++;
        oid = atoi(oname);
    }

    fprintf(stderr, "Using container %s object %u\n", cname, oid);

    for (i = 0; i < MAX_CHUNKS; i++) {
        if (strcmp(containers[i].name, cname) == 0) {
            stbuf->st_size = containers[i].size[oid];
            break;
        }
    }

    free(cname);
    free(fname);

	return (i == MAX_CHUNKS ? -1 : 0);
}

static int xmp_access(const char *path, int mask)
{
	int res;

	res = access(path, mask);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_readlink(const char *path, char *buf, size_t size)
{
	int res;

	res = readlink(path, buf, size - 1);
	if (res == -1)
		return -errno;

	buf[res] = '\0';
	return 0;
}


static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
		       off_t offset, struct fuse_file_info *fi)
{
	DIR *dp;
	struct dirent *de;

	(void) offset;
	(void) fi;

	dp = opendir(path);
	if (dp == NULL)
		return -errno;

	while ((de = readdir(dp)) != NULL) {
		struct stat st;
		memset(&st, 0, sizeof(st));
		st.st_ino = de->d_ino;
		st.st_mode = de->d_type << 12;
		if (filler(buf, de->d_name, &st, 0))
			break;
	}

	closedir(dp);
	return 0;
}

static int xmp_mknod(const char *path, mode_t mode, dev_t rdev)
{
	int res;

	/* On Linux this could just be 'mknod(path, mode, rdev)' but this
	   is more portable */
	if (S_ISREG(mode)) {
		res = open(path, O_CREAT | O_EXCL | O_WRONLY, mode);
		if (res >= 0)
			res = close(res);
	} else if (S_ISFIFO(mode))
		res = mkfifo(path, mode);
	else
		res = mknod(path, mode, rdev);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
	int res;

	res = mkdir(path, mode);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_unlink(const char *path)
{
	int res;

	res = unlink(path);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_rmdir(const char *path)
{
	int res;

	res = rmdir(path);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_symlink(const char *from, const char *to)
{
	int res;

	res = symlink(from, to);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_rename(const char *from, const char *to)
{
	int res;

	res = rename(from, to);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_link(const char *from, const char *to)
{
	int res;

	res = link(from, to);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_chmod(const char *path, mode_t mode)
{
	int res;

	res = chmod(path, mode);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_chown(const char *path, uid_t uid, gid_t gid)
{
	int res;

	res = lchown(path, uid, gid);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_truncate(const char *path, off_t size)
{
	int res;

	res = truncate(path, size);
	if (res == -1)
		return -errno;

	return 0;
}

#ifdef HAVE_UTIMENSAT
static int xmp_utimens(const char *path, const struct timespec ts[2])
{
	int res;

	/* don't use utime/utimes since they follow symlinks */
	res = utimensat(0, path, ts, AT_SYMLINK_NOFOLLOW);
	if (res == -1)
		return -errno;

	return 0;
}
#endif

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
	int res;

    RETRY(open, path, fi->flags);
#if 0
	res = open(path, fi->flags);
	if (res == -1)
		return -errno;
#endif

	close(res);
	return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset,
		    struct fuse_file_info *fi)
{
	int fd;
	int res;
    struct stat stbuf;

    //fprintf(stderr, "DBG: read %s\n", path);


    // if file size is 0, fetch from swift
	res = lstat(path, &stbuf);
	if (res == -1) {
        fprintf(stderr, "lstat failed on file %s in xmpread\n", path);
		return -errno;
    }

    if (!stbuf.st_size)
        swift_fetch(path);

	(void) fi;
	fd = open(path, O_RDONLY);
	if (fd == -1) {
		    return -errno;
    }

	res = pread(fd, buf, size, offset);
	if (res == -1)
		res = -errno;

	close(fd);
	return res;
}

static int xmp_write(const char *path, const char *buf, size_t size,
		     off_t offset, struct fuse_file_info *fi)
{
	int fd;
	int res;

    fprintf(stderr, "DBG: write %s\n", path);

	(void) fi;
	fd = open(path, O_WRONLY);
	if (fd == -1)
		return -errno;

	res = pwrite(fd, buf, size, offset);
	if (res == -1)
		res = -errno;

	close(fd);
	return res;
}

static int xmp_statfs(const char *path, struct statvfs *stbuf)
{
	int res;

	res = statvfs(path, stbuf);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_release(const char *path, struct fuse_file_info *fi)
{
	/* Just a stub.	 This method is optional and can safely be left
	   unimplemented */

	(void) path;
	(void) fi;
	return 0;
}

static int xmp_fsync(const char *path, int isdatasync,
		     struct fuse_file_info *fi)
{
	/* Just a stub.	 This method is optional and can safely be left
	   unimplemented */

	(void) path;
	(void) isdatasync;
	(void) fi;
	return 0;
}

#ifdef HAVE_POSIX_FALLOCATE
static int xmp_fallocate(const char *path, int mode,
			off_t offset, off_t length, struct fuse_file_info *fi)
{
	int fd;
	int res;

	(void) fi;

	if (mode)
		return -EOPNOTSUPP;

	fd = open(path, O_WRONLY);
	if (fd == -1)
		return -errno;

	res = -posix_fallocate(fd, offset, length);

	close(fd);
	return res;
}
#endif

#ifdef HAVE_SETXATTR
/* xattr operations are optional and can safely be left unimplemented */
static int xmp_setxattr(const char *path, const char *name, const char *value,
			size_t size, int flags)
{
	int res = lsetxattr(path, name, value, size, flags);
	if (res == -1)
		return -errno;
	return 0;
}

static int xmp_getxattr(const char *path, const char *name, char *value,
			size_t size)
{
	int res = lgetxattr(path, name, value, size);
	if (res == -1)
		return -errno;
	return res;
}

static int xmp_listxattr(const char *path, char *list, size_t size)
{
	int res = llistxattr(path, list, size);
	if (res == -1)
		return -errno;
	return res;
}

static int xmp_removexattr(const char *path, const char *name)
{
	int res = lremovexattr(path, name);
	if (res == -1)
		return -errno;
	return 0;
}
#endif /* HAVE_SETXATTR */

static struct fuse_operations xmp_oper = {
	.getattr	= xmp_getattr,
	.access		= xmp_access,
	.readlink	= xmp_readlink,
	.readdir	= xmp_readdir,
	.mknod		= xmp_mknod,
	.mkdir		= xmp_mkdir,
	.symlink	= xmp_symlink,
	.unlink		= xmp_unlink,
	.rmdir		= xmp_rmdir,
	.rename		= xmp_rename,
	.link		= xmp_link,
	.chmod		= xmp_chmod,
	.chown		= xmp_chown,
	.truncate	= xmp_truncate,
#ifdef HAVE_UTIMENSAT
	.utimens	= xmp_utimens,
#endif
	.open		= xmp_open,
	.read		= xmp_read,
	.write		= xmp_write,
	.statfs		= xmp_statfs,
	.release	= xmp_release,
	.fsync		= xmp_fsync,
#ifdef HAVE_POSIX_FALLOCATE
	.fallocate	= xmp_fallocate,
#endif
#ifdef HAVE_SETXATTR
	.setxattr	= xmp_setxattr,
	.getxattr	= xmp_getxattr,
	.listxattr	= xmp_listxattr,
	.removexattr	= xmp_removexattr,
#endif
};

int main(int argc, char *argv[])
{
	umask(0);

    msgq_client_init();

	return fuse_main(argc, argv, &xmp_oper, NULL);
}
