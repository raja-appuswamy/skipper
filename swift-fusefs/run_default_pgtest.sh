#!/bin/bash

export Swift_Auth_Token=`(curl -v -H 'X-Storage-User: test:tester' -H 'X-Storage-Pass: testing' "http://127.0.0.1:8080/auth/v1.0") 2>&1 > /dev/null | grep "X-Auth-Token" | cut -d':' -f2 | tr -d '[:space:]'`

export Swift_Storage_URL='http://127.0.0.1:8080/v1/AUTH_test'

function restart_scheduler {

    kill -9 `ps ax | grep client-scheduler | head -1 | tr -s '  ' ' ' | cut -d' ' -f1`

    cd /media/Data1/code/cold-storage/client_scheduler/scheduler

    for i in {1,2};
    do
        sudo ipcrm -Q `ipcs | tail -2 | head -1 | cut -d' ' -f1`;
    done

    sudo rm /tmp/query.json /tmp/cli_key /tmp/srv_key

    for file in `ls /media/Data2/fuse/`; do >/media/Data2/fuse/$file; done

    ./client-scheduler 1 &

    cd -
}


for q in 1 4 6 7 12
do

    restart_scheduler

    sudo ./fusexmp -o modules=subdir,subdir=/media/Data2/fuse,allow_other,default_permissions /media/Data2/data/pgsql/swift_space/PG_9.2_201204301/12042/

    echo Running query $q

    echo query $i start >> results.log 2>&1

    date >> results.log 2>&1

    /usr/lib/postgresql/9.2.1/bin/psql -U postgres -d postgres -f /media/Data1/code/cold-storage/queries/q$q.sql -o r$q.out

    date >> results.log 2>&1

    echo query $i done >> results.log 2>&1

    echo Done query $q

    sudo fusermount -u /media/Data2/data/pgsql/swift_space/PG_9.2_201204301/12042/ 

done
