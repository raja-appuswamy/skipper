Project Skipper
---------------

As enterprises accumulate vast amounts of information driven by the desire to
extract actionable insights,  cost-effective storage of data has become very
important to minimize the cost of data analytics. Recently storage vendors have
developed Cold Storage Devices (CSD) with worst-case access latencies between
hard disks (microseconds) and tape drives (minutes) to minimize data storage
cost. The Skipper project focuses on query execution techniques for deploying
analytical databases on CSD.
